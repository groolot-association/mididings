PACKAGE = mididings

.PHONY: deb pip all build-deb deb-pkg-deps install install-deb install-debian-pip-deps build-pip install-pip clean

all: deb pip

deb: build-deb
	mv ../*$(PACKAGE)[-_]* ./
build-deb:
	dpkg-buildpackage --no-sign --jobs-force
deb-deps: /etc/debian_version
	@sudo apt-get -qq update
	sudo apt-get install build-essential debhelper dh-python libasound2-dev libboost-python-dev libboost-thread-dev libglib2.0-dev libjack-jackd2-dev libsmf-dev python3-all-dev python3-setuptools python3-sphinx

install: install-deb
install-deb: deb
	apt install ./*.deb


pip: build-pip
deb-pip-deps: /etc/debian_version
	@sudo apt-get -qq update
	@sudo apt-get -qq install liblo-dev python3-pip dh-python python3-setuptools

build-pip: setup.py
	pip3 wheel .

install-pip: build-pip
	pip3 install $(PACKAGE)*.whl


clean:
	-rm -rf build/ *$(PACKAGE)[-_]*
